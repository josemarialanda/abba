package com.landagmail.josemaria.appprojectreligion.Fragments.BibleContentChooserFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.landagmail.josemaria.appprojectreligion.Adapters.BooksAdapter;
import com.landagmail.josemaria.appprojectreligion.DataBase.SqlLiteDbHelperBIBLE;
import com.landagmail.josemaria.appprojectreligion.R;

import java.util.ArrayList;

public class BooksFragment extends Fragment {

    public BooksFragment() {
    }

    public RecyclerView bookList;
    private BooksAdapter adapter;
    ArrayList<String> books;
    SqlLiteDbHelperBIBLE dbHelperBIBLE;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbHelperBIBLE = new SqlLiteDbHelperBIBLE(getActivity());
        dbHelperBIBLE.openDataBase();
        books = (ArrayList<String>) dbHelperBIBLE.getBookRows();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_books, container, false);

        bookList = (RecyclerView) view.findViewById(R.id.booksList);
        bookList.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new BooksAdapter(getActivity(), BooksFragment.this);
        adapter.setBooksData(books);
        bookList.setAdapter(adapter);

        return view;
    }
}
