package com.landagmail.josemaria.appprojectreligion.Utils.BlurUtils.AsynchronousBlur;

//HOW TO USE

/*

Overlay
Parent must be ViewGroup:

Blurry.with(context).radius(25).sampling(2).onto(rootView);

----------------------------------------------------------------

Into

Blurry.with(context).capture(view).into(imageView);

----------------------------------------------------------------

Blur Options

Radius
Down Sampling
Color Filter
Asynchronous Support
Blurry.with(context)
  .radius(10)
  .sampling(8)
  .color(Color.argb(66, 255, 255, 0))
  .async()
  .onto(rootView);

*/


import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.landagmail.josemaria.appprojectreligion.Utils.BlurUtils.AsynchronousBlur.Internal.Blur;
import com.landagmail.josemaria.appprojectreligion.Utils.BlurUtils.AsynchronousBlur.Internal.BlurFactor;
import com.landagmail.josemaria.appprojectreligion.Utils.BlurUtils.AsynchronousBlur.Internal.BlurTask;
import com.landagmail.josemaria.appprojectreligion.Utils.BlurUtils.AsynchronousBlur.Internal.Helper;

public class Blurry {

    private static final String TAG = Blurry.class.getSimpleName();

    public static Composer with(Context context) {
        return new Composer(context);
    }

    public static void delete(ImageView image) {
        image.setImageDrawable(null);
    }

    public static void delete(ViewGroup group) {
        View view = group.findViewWithTag(TAG);
        if (view != null) {
            group.removeView(view);
        }
    }

    public static class Composer {

        private View blurredView;
        private Context context;
        private BlurFactor factor;
        private boolean async;

        public Composer(Context context) {
            this.context = context;
            blurredView = new View(context);
            blurredView.setTag(TAG);
            factor = new BlurFactor();
        }

        public Composer radius(int radius) {
            factor.radius = radius;
            return this;
        }

        public Composer sampling(int sampling) {
            factor.sampling = sampling;
            return this;
        }

        public Composer color(int color) {
            factor.color = color;
            return this;
        }

        public Composer async() {
            this.async = true;
            return this;
        }

        public ImageComposer capture(View capture) {
            return new ImageComposer(context, capture, factor, async);
        }

        public void onto(final View target) {
            if (target instanceof ViewGroup) {
                factor.width = target.getMeasuredWidth();
                factor.height = target.getMeasuredHeight();

                if (async) {
                    BlurTask.execute(target, factor, new BlurTask.Callback() {
                        @Override
                        public void done(BitmapDrawable drawable) {
                            Helper.setBackground(blurredView, drawable);
                            ((ViewGroup) target).addView(blurredView);
                        }
                    });
                } else {
                    Drawable drawable =
                            new BitmapDrawable(context.getResources(), Blur.rs(target, factor));

                    Helper.setBackground(blurredView, drawable);
                    ((ViewGroup) target).addView(blurredView);
                }
            } else {
                throw new IllegalArgumentException("View parent must be ViewGroup");
            }
        }
    }

    public static class ImageComposer {

        private Context context;
        private View capture;
        private BlurFactor factor;
        private boolean async;

        public ImageComposer(Context context, View capture, BlurFactor factor, boolean async) {
            this.context = context;
            this.capture = capture;
            this.factor = factor;
            this.async = async;
        }

        public void into(final ImageView target) {
            factor.width = capture.getMeasuredWidth();
            factor.height = capture.getMeasuredHeight();

            if (async) {
                BlurTask.execute(capture, factor, new BlurTask.Callback() {
                    @Override
                    public void done(BitmapDrawable drawable) {
                        target.setImageDrawable(drawable);
                    }
                });
            } else {
                Drawable drawable =
                        new BitmapDrawable(context.getResources(), Blur.rs(capture, factor));
                target.setImageDrawable(drawable);
            }
        }
    }
}