package com.landagmail.josemaria.appprojectreligion.Activities;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.landagmail.josemaria.appprojectreligion.Logging.L;
import com.landagmail.josemaria.appprojectreligion.R;

import java.util.ArrayList;

public class BibleSearchResultsActivity extends ActionBarActivity {

    ArrayList<String> titles;
    ArrayList<String> previews;

    private RecyclerView searchResultsList;
    private SearchResultsAdapter adapter;

    public BibleSearchResultsActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bible_search_results);

        titles = getIntent().getBundleExtra("searchData").getStringArrayList("titlesArray");
        previews = getIntent().getBundleExtra("searchData").getStringArrayList("previewsArray");

        searchResultsList = (RecyclerView) findViewById(R.id.searchResultsList);
        searchResultsList.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SearchResultsAdapter(this);
        adapter.setData(titles, previews);
        searchResultsList.setAdapter(adapter);

    }

    private class SearchResultsAdapter extends RecyclerView.Adapter<SearchResultsAdapter.myViewHolder> implements View.OnClickListener {

        private LayoutInflater layoutInflater;
        ArrayList<String> titles;
        ArrayList<String> previews;
        Context context;

        public SearchResultsAdapter(BibleSearchResultsActivity context) {
            layoutInflater = LayoutInflater.from(context);
            this.context = context;
        }

        @Override
        public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = layoutInflater.inflate(R.layout.bible_search_results_list_view, parent, false);
            myViewHolder viewHolder = new myViewHolder(view);
            view.setOnClickListener(this);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(myViewHolder holder, int position) {
            String title = titles.get(position);
            String preview = previews.get(position);

            holder.searchTitle.setText(title);
            holder.searchPreview.setText(preview);
        }

        @Override
        public int getItemCount() {
            return titles.size();
        }

        public void setData(ArrayList<String> titles, ArrayList<String> previews) {
            this.titles = titles;
            this.previews = previews;
            notifyDataSetChanged();
        }

        @Override
        public void onClick(View v) {
            int position = searchResultsList.getChildPosition(v);
            final String title = titles.get(position);
            final String preview = previews.get(position);

            PopupMenu popup = new PopupMenu(BibleSearchResultsActivity.this, v);
            popup.getMenuInflater().inflate(R.menu.popup_menu_bible_search, popup.getMenu());
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {

                    switch (item.getItemId()) {
                        case R.id.mailShare:
                            emailIntent(title, preview);
                            break;
                        case R.id.whatsappShare:
                            whatsappIntent(title, preview);
                            break;
                        case R.id.copy:
                            copyBibleFragment(title, preview);
                            break;
                        case R.id.search:
                            searchBible(title);
                            break;
                    }

                    return true;
                }
            });
            popup.show();
        }

        private void emailIntent(String title, String preview) {

            String data = title + "\n\n" + preview;

            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:"));
            intent.putExtra(Intent.EXTRA_SUBJECT, "Buscador de biblia Abba");
            intent.putExtra(Intent.EXTRA_TEXT, data + "\n\n" + "Enterate de tu religion con abba!" + "\n" + "Descarga abba en el play store para leer mas contenido");
            startActivity(Intent.createChooser(intent, "Email via..."));
        }
        private void whatsappIntent(String title, String preview) {

            String data = title + "\n\n" + preview;

            PackageManager pm = getPackageManager();
            try {
                Intent waIntent = new Intent(Intent.ACTION_SEND);
                waIntent.setType("text/plain");
                String text = data + "\n\n" + "Enterate de tu religion con abba!"  + "\n" + "Descarga abba en el play store para leer mas contenido";
                PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                waIntent.setPackage("com.whatsapp");
                waIntent.putExtra(Intent.EXTRA_TEXT, text);
                startActivity(Intent.createChooser(waIntent, "Share with"));

            } catch (PackageManager.NameNotFoundException e) {
                Toast.makeText(BibleSearchResultsActivity.this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                        .show();
            }
        }
        private void copyBibleFragment(String title, String preview) {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("texto de biblia", title + "\n\n" + preview);
            clipboard.setPrimaryClip(clip);
        }
        private void searchBible(String title) {
            Intent intent = new Intent(context, BibleSearchContent.class);
            intent.putExtra("title", title);
            startActivity(intent);
        }

        class myViewHolder extends RecyclerView.ViewHolder {

            TextView searchTitle;
            TextView searchPreview;

            public myViewHolder(View itemView) {
                super(itemView);
                searchTitle = (TextView) itemView.findViewById(R.id.searchTitle);
                searchPreview = (TextView) itemView.findViewById(R.id.searchPreview);
            }
        }
    }
}
