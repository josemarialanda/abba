package com.landagmail.josemaria.appprojectreligion.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.landagmail.josemaria.appprojectreligion.Logging.L;
import com.landagmail.josemaria.appprojectreligion.Network.VolleySingleton;
import com.landagmail.josemaria.appprojectreligion.R;
import com.nineoldandroids.view.ViewHelper;

import java.util.List;

public class NewsActivity extends ActionBarActivity implements ObservableScrollViewCallbacks {

    private ImageView mImageView;
    private View mToolbarView;
    private ObservableScrollView mScrollView;
    private int mParallaxImageHeight;
    private TextView body;
    private TextView titleTextView;

    private String title;
    private String content;
    private String url;
    private String link;

    private ImageLoader mImageLoader;
    private VolleySingleton mVolleySingleton;

    private SharedPreferences textSizePrefs;
    private static final String FONT_SIZE_PREFS = "textPrefs";
    private SharedPreferences.Editor TextEditorPrefs;
    private static final String FONT_SIZE = "textSize";

    private int TextSize = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);


        mVolleySingleton = VolleySingleton.getInstance();
        mImageLoader = mVolleySingleton.getImageLoader();

        setSupportActionBar((Toolbar) findViewById(R.id.app_bar));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        mImageView = (ImageView) findViewById(R.id.image);
        titleTextView = (TextView) findViewById(R.id.title);
        body = (TextView) findViewById(R.id.body);
        mToolbarView = findViewById(R.id.app_bar);
        mToolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(0, getResources().getColor(R.color.primary)));
        mScrollView = (ObservableScrollView) findViewById(R.id.scroll);
        mScrollView.setScrollViewCallbacks(this);
        mParallaxImageHeight = getResources().getDimensionPixelSize(R.dimen.parallax_image_height);

        url = getIntent().getBundleExtra("DisplayDataBundle").getString("image", null);
        content = getIntent().getBundleExtra("DisplayDataBundle").getString("content", null);
        title = getIntent().getBundleExtra("DisplayDataBundle").getString("title", null);
        link = getIntent().getBundleExtra("DisplayDataBundle").getString("link", null);

        titleTextView.setText(title);
        body.setText(Html.fromHtml(content));
        loadImages(url);

        readPreferences();
    }

    private void loadImages(String urlThumbnail) {
        mImageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                mImageView.setImageBitmap(response.getBitmap());
            }

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onScrollChanged(mScrollView.getCurrentScrollY(), false, false);
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        int baseColor = getResources().getColor(R.color.primary);
        float alpha = Math.min(1, (float) scrollY / mParallaxImageHeight);
        mToolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
        ViewHelper.setTranslationY(mImageView, scrollY / 2);
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }

    private void twitterIntent(){

        String contentUrl = String.valueOf(Uri.parse(link));
        String contentTitle = title;

        Intent tweetIntent = new Intent(Intent.ACTION_SEND);
        tweetIntent.putExtra(Intent.EXTRA_TEXT, contentTitle + "\n\n" + contentUrl + "\n\n" + "Descarga abba para leer mas noticias como esta");
        tweetIntent.setType("text/plain");

        PackageManager packManager = getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(tweetIntent,  PackageManager.MATCH_DEFAULT_ONLY);

        boolean resolved = false;
        for(ResolveInfo resolveInfo: resolvedInfoList){
            if(resolveInfo.activityInfo.packageName.startsWith("com.twitter.android")){
                tweetIntent.setClassName(
                        resolveInfo.activityInfo.packageName,
                        resolveInfo.activityInfo.name );
                resolved = true;
                break;
            }
        }
        if(resolved){
            startActivity(tweetIntent);
        }else{
            Toast.makeText(this, "Twitter app isn't found", Toast.LENGTH_LONG).show();
        }
    }

    private void emailIntent() {

        String data = String.valueOf(Html.fromHtml(content));

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_SUBJECT, title);
        intent.putExtra(Intent.EXTRA_TEXT, data + "\n\n" + "Enterate de tu religion con abba!" + "\n" + "Descarga abba en el play store para leer mas noticias como esta");
        startActivity(Intent.createChooser(intent, "Email via..."));
    }
    private void whatsappIntent() {
        String data = String.valueOf(Html.fromHtml(content));
        PackageManager pm=getPackageManager();
        try {
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            String text = title + "\n\n" + data + "\n\n" + "Enterate de tu religion con abba!"  + "\n" + "Descarga abba en el play store para leer mas noticias como esta";
            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(waIntent, "Share with"));

        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                    .show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_news, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (item.getItemId()){
            case R.id.twitterShare:
                twitterIntent();
                break;
            case R.id.facebookShare:
                L.m("Facebook!");
                break;
            case R.id.mailShare:
                emailIntent();
                break;
            case R.id.whatsappShare:
                whatsappIntent();
                break;
        }

        if (id == R.id.fontBigger) {
            if (TextSize > 13 && TextSize <25){
                TextSize++;
                body.setTextSize(TextSize);
                //L.t(String.valueOf(TextSize));
            }
        }
        if (id == R.id.fontSmaller) {
            if (TextSize > 13 && TextSize <25){
                TextSize--;
                body.setTextSize(TextSize);
                //L.t(String.valueOf(TextSize));
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        textSizePrefs = getSharedPreferences(FONT_SIZE_PREFS, Context.MODE_PRIVATE);
        TextEditorPrefs = textSizePrefs.edit();
        TextEditorPrefs.putInt(FONT_SIZE, TextSize);
        TextEditorPrefs.apply();
        Log.i("cool", "onPause " + TextSize);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("cool", "onResume " + TextSize);
    }

    private void readPreferences() {
        textSizePrefs = getSharedPreferences(FONT_SIZE_PREFS, Context.MODE_PRIVATE);
        TextSize = textSizePrefs.getInt(FONT_SIZE, 20);
        body.setTextSize(TextSize);
    }
}
