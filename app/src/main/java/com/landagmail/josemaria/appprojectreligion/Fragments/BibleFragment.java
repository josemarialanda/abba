package com.landagmail.josemaria.appprojectreligion.Fragments;
//BibleFragment

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;

import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.landagmail.josemaria.appprojectreligion.Activities.BibleContentChooserActivity;
import com.landagmail.josemaria.appprojectreligion.Activities.BibleSearchResultsActivity;

import com.landagmail.josemaria.appprojectreligion.Activities.UsersActivities.SignInActivity;
import com.landagmail.josemaria.appprojectreligion.BaseClasses.BaseFragment;
import com.landagmail.josemaria.appprojectreligion.Constants;
import com.landagmail.josemaria.appprojectreligion.DataBase.SqlLiteDbHelperBIBLE;
import com.landagmail.josemaria.appprojectreligion.Logging.L;
import com.landagmail.josemaria.appprojectreligion.Network.VolleySingleton;
import com.landagmail.josemaria.appprojectreligion.POJO.BibleUtils.BibleSearchData;
import com.landagmail.josemaria.appprojectreligion.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BibleFragment extends BaseFragment implements View.OnClickListener {

    private int DATA_REQUEST_CODE = 0;

    TextView bibleContentTextView;
    ImageButton backButton;
    Button passageButton;
    Button searchButton;
    ImageButton nextButton;
    private ObservableScrollView scrollView;
    public static final String ARG_SCROLL_Y = "ARG_SCROLL_Y";

    private final static String FONT_SIZE_PREFS = "com.landagmail.josemaria.appprojectreligion.Fragments.FONT_SIZE_PREFS";
    private final static String FONT_SIZE = "com.landagmail.josemaria.appprojectreligion.Fragments.FONT_SIZE";
    private SharedPreferences textSizePrefs;

    private final static String BOOK_PREFS = "com.landagmail.josemaria.appprojectreligion.Fragments.BOOK_PREFS";
    private final static String BOOK_SELECTED = "com.landagmail.josemaria.appprojectreligion.Fragments.BOOK_SELECTED";
    private final static String CHAPTER_SELECTED = "com.landagmail.josemaria.appprojectreligion.Fragments.CHAPTER_SELECTED";
    private final static String BIBLE_TEXT = "com.landagmail.josemaria.appprojectreligion.Fragments.CHAPTER_SELECTED.BIBLE_TEXT";
    private SharedPreferences bookContentPrefs;

    private String book;
    private int chapter;
    private String content;
    private int TextSize = 20;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_CANCELED) {
            return;
        }

        if (requestCode == DATA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                book = data.getBundleExtra("displayDataInfoBundle").getString("book");
                chapter = data.getBundleExtra("displayDataInfoBundle").getInt("chapter");

                SqlLiteDbHelperBIBLE dbHelperBIBLE = new SqlLiteDbHelperBIBLE(getActivity());
                dbHelperBIBLE.openDataBase();
                content = dbHelperBIBLE.getBibleContent(book, chapter);
                bibleContentTextView.setText(content);

            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bible, container, false);
        bibleContentTextView = (TextView) view.findViewById(R.id.bibleContentTextView);
        readPreferences();
        backButton = (ImageButton) view.findViewById(R.id.backButton);
        backButton.setOnClickListener(this);
        passageButton = (Button) view.findViewById(R.id.passageButton);
        passageButton.setOnClickListener(this);
        searchButton = (Button) view.findViewById(R.id.searchButton);
        searchButton.setOnClickListener(this);
        nextButton = (ImageButton) view.findViewById(R.id.nextButton);
        nextButton.setOnClickListener(this);

        scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
        Activity parentActivity = getActivity();
        if (parentActivity instanceof ObservableScrollViewCallbacks) {
            // Scroll to the specified offset after layout
            Bundle args = getArguments();
            if (args != null && args.containsKey(ARG_SCROLL_Y)) {
                final int scrollY = args.getInt(ARG_SCROLL_Y, 0);
                ScrollUtils.addOnGlobalLayoutListener(scrollView, new Runnable() {
                    @Override
                    public void run() {
                        scrollView.scrollTo(0, scrollY);
                    }
                });
            }

            scrollView.setTouchInterceptionViewGroup((ViewGroup) parentActivity.findViewById(R.id.root));

            scrollView.setScrollViewCallbacks((ObservableScrollViewCallbacks) parentActivity);
        }
        return view;
    }

    private void readPreferences() {
        textSizePrefs = getActivity().getSharedPreferences(FONT_SIZE_PREFS, Context.MODE_PRIVATE);
        TextSize = textSizePrefs.getInt(FONT_SIZE, 20);
        bibleContentTextView.setTextSize(TextSize);

        bookContentPrefs = getActivity().getSharedPreferences(BOOK_PREFS, Context.MODE_PRIVATE);
        book = bookContentPrefs.getString(BOOK_SELECTED, null);
        chapter = bookContentPrefs.getInt(CHAPTER_SELECTED, 0);
        content = bookContentPrefs.getString(BIBLE_TEXT, "Click on passage to read the bible");
        bibleContentTextView.setText(content);
    }

    @Override
    public void onPause() {
        super.onPause();
        textSizePrefs = getActivity().getSharedPreferences(FONT_SIZE_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor textEditorPrefs = textSizePrefs.edit();
        textEditorPrefs.clear();
        textEditorPrefs.putInt(FONT_SIZE, TextSize);
        textEditorPrefs.apply();
        Log.i("cool", "onPause " + TextSize);

        bookContentPrefs = getActivity().getSharedPreferences(BOOK_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor bookEditorPrefs = bookContentPrefs.edit();
        bookEditorPrefs.clear();
        bookEditorPrefs.putString(BOOK_SELECTED, book);
        bookEditorPrefs.putInt(CHAPTER_SELECTED, chapter);
        bookEditorPrefs.putString(BIBLE_TEXT, content);
        bookEditorPrefs.apply();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("cool", "onResume " + TextSize);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_bible_fragment, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.fontSizeBigger) {
            if (TextSize > 13 && TextSize < 25) {
                TextSize++;
                bibleContentTextView.setTextSize(TextSize);
                //L.t(String.valueOf(TextSize));
            }
        }
        if (id == R.id.fontSizeSmaller) {
            if (TextSize > 13 && TextSize < 25) {
                TextSize--;
                bibleContentTextView.setTextSize(TextSize);
                //L.t(String.valueOf(TextSize));
            }
        }
        if (id == R.id.action_settings) {
            //Intent intent= new Intent(getActivity(), SettingsActivity.class);
            Intent intent = new Intent(getActivity(), SignInActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backButton:
                Toast.makeText(getActivity(), book + chapter--, Toast.LENGTH_LONG).show();
                break;
            case R.id.passageButton:
                Intent intent = new Intent(getActivity(), BibleContentChooserActivity.class);
                startActivityForResult(intent, DATA_REQUEST_CODE);
                break;
            case R.id.searchButton:

                if (isOnline()) {
                    searchBible();
                } else {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Red inalámbrica no detectada", Snackbar.LENGTH_LONG)
                            .setAction("Conectarse", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                                }
                            })
                            .setActionTextColor(Color.RED)
                            .show();
                }


                break;
            case R.id.nextButton:
                Toast.makeText(getActivity(), book + chapter++, Toast.LENGTH_LONG).show();
                break;
        }
    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private RadioGroup radioGroup;
    private EditText querryTxt;
    private View positiveAction;
    private String sort;

    private void searchBible() {

        MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title("Bible search")
                .icon(getResources().getDrawable(R.drawable.ic_action_book))
                .customView(R.layout.custom_search_bible_dialog, true)
                .positiveText("Search")
                .negativeText("Cancel")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        doCustomSearch(querryTxt.getText().toString(),
                                sort);
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                    }
                }).build();

        positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
        querryTxt = (EditText) dialog.getCustomView().findViewById(R.id.querryTxt);

        radioGroup = (RadioGroup) dialog.getCustomView().findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioButtonRelevance:
                        positiveAction.setEnabled(true);
                        sort = "relevance";
                        break;
                    case R.id.radioButtonPassage:
                        sort = "passage";
                        positiveAction.setEnabled(true);
                        break;
                }
            }
        });

        dialog.show();
        positiveAction.setEnabled(false); // disabled by default
    }

    private void doCustomSearch(String string, String sort) {
        new CustomSearchTask().execute(string, sort);
    }

    private class CustomSearchTask extends AsyncTask<String, String, String> {

        RequestQueue requestqueue;
        String searchRequestUrl;
        BibleSearchData searchData;
        Context context;

        private void fetchDisplayData(String querry, String sort) {
            setRequestUrl(querry, sort);
            requestqueue = VolleySingleton.getInstance().getRequestQueue();
        }

        private void setRequestUrl(String querry, String s) {

            querry = querry.replace(" ", "%20").trim();

            searchRequestUrl = Constants.BIBLE_SEARCH_PART_1 +
                    Constants.BIBLE_RVA + Constants.BIBLE_SEARCH_PART_2 +
                    querry +
                    Constants.BIBLE_SEARCH_PART_4 +
                    Constants.BIBLE_API_KEY +
                    Constants.CULTURE_SPEC;
        }

        private String getSearchRequestUrl() {
            return searchRequestUrl;
        }

        @Override
        protected String doInBackground(String... params) {
            fetchDisplayData(params[0], params[1]);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, getSearchRequestUrl()
                    , new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    if (response == null || response.length() == 0) {
                        return;
                    } else {
                        try {
                            JSONArray results = response.getJSONArray("results");
                            searchData = new BibleSearchData();

                            for (int i = 0; i < results.length(); i++) {
                                String title = results.getJSONObject(i).getString("title");
                                String preview = results.getJSONObject(i).getString("preview");
                                searchData.setTitle(title);
                                searchData.setPreview(preview);
                            }

                            onProgressUpdate("doneLoading");

                        } catch (JSONException e) {
                            L.m(e.getMessage());
                        }

                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            requestqueue.add(request);

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            Intent intent = new Intent(context, BibleSearchResultsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putStringArrayList("titlesArray", searchData.getTitles());
            bundle.putStringArrayList("previewsArray", searchData.getPreviews());
            intent.putExtra("searchData", bundle);
            startActivity(intent);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            context = getActivity();
        }
    }
}
