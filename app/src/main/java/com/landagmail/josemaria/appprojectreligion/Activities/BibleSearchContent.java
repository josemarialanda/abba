package com.landagmail.josemaria.appprojectreligion.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.landagmail.josemaria.appprojectreligion.Constants;
import com.landagmail.josemaria.appprojectreligion.Logging.L;
import com.landagmail.josemaria.appprojectreligion.Network.VolleySingleton;
import com.landagmail.josemaria.appprojectreligion.POJO.BibleUtils.BibleSearchData;
import com.landagmail.josemaria.appprojectreligion.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BibleSearchContent extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView bibleContentTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bible_search_content);

        String data = getIntent().getStringExtra("title");

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(data);

        bibleContentTextView = (TextView) findViewById(R.id.bibleContentTextView);

        new CustomSearchTask().execute(data);
    }

    private class CustomSearchTask extends AsyncTask<String, String, String> {

        RequestQueue requestqueue;
        String searchRequestUrl;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        private void fetchDisplayData(String querry, String sort) {
            setRequestUrl(querry, sort);
            requestqueue = VolleySingleton.getInstance().getRequestQueue();
        }

        private void setRequestUrl(String querry, String s) {

            querry = querry.replace(" ", "%20").trim();

            searchRequestUrl = Constants.BIBLE_SEARCH_PART_1 +
                    Constants.BIBLE_RVA + Constants.BIBLE_SEARCH_PART_2 +
                    querry +
                    Constants.BIBLE_SEARCH_PART_4 +
                    Constants.BIBLE_API_KEY +
                    Constants.CULTURE_SPEC;
        }

        private String getSearchRequestUrl() {
            return searchRequestUrl;
        }

        @Override
        protected String doInBackground(String... params) {
            fetchDisplayData(params[0], params[1]);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, getSearchRequestUrl()
                    , new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {

                    if (response == null || response.length() == 0) {
                        return;
                    } else {
                        try {
                            JSONArray results = response.getJSONArray("results");


                        } catch (JSONException e) {
                            L.m(e.getMessage());
                        }

                    }
                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            requestqueue.add(request);

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
           bibleContentTextView.setText(values[0]);
        }
    }
}
