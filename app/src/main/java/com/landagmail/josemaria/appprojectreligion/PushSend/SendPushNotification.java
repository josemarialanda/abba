package com.landagmail.josemaria.appprojectreligion.PushSend;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.landagmail.josemaria.appprojectreligion.Activities.Lecturas;
import com.landagmail.josemaria.appprojectreligion.Activities.NewsActivity;
import com.landagmail.josemaria.appprojectreligion.Activities.Prayers;
import com.landagmail.josemaria.appprojectreligion.Constants;
import com.landagmail.josemaria.appprojectreligion.Logging.L;
import com.landagmail.josemaria.appprojectreligion.MainActivity;
import com.landagmail.josemaria.appprojectreligion.MyApplication;
import com.landagmail.josemaria.appprojectreligion.Network.SimpleXmlRequest;
import com.landagmail.josemaria.appprojectreligion.Network.VolleySingleton;
import com.landagmail.josemaria.appprojectreligion.POJO.RssCommon.RSS;
import com.landagmail.josemaria.appprojectreligion.R;

public class SendPushNotification {

    //url = getIntent().getBundleExtra("DisplayDataBundle").getString("image", null);
    //content = getIntent().getBundleExtra("DisplayDataBundle").getString("content", null);
    //title = getIntent().getBundleExtra("DisplayDataBundle").getString("title", null);

    private static String title;
    private static String content;
    private static String imageUrl;

    private static int mid = 0;

    static Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

    public static void dailyNewsPush() {

        RequestQueue queue = VolleySingleton.getInstance().getRequestQueue();

        SimpleXmlRequest<RSS> simpleRequest = new SimpleXmlRequest<RSS>(Request.Method.GET, Constants.NEWS_FEED_URL_MUNDO, RSS.class,
                new Response.Listener<RSS>() {
                    @Override
                    public void onResponse(RSS response) {
                        title = response.getChannel().itemList.get(0).getTitle();
                        imageUrl = response.getChannel().itemList.get(0).getEnclosure().getUrl();
                        content = response.getChannel().itemList.get(0).getEncoded().replaceAll("http:\\/\\/(.*?)[^\\\"']+", "")
                                .replace("<img src=\"\" />", "")
                                .replace("<img src=\"\" height=\"1\" width=\"1\" alt=\"\"/>", "");

                        sendShortPush(title, content, uri, imageUrl);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        L.t(error.getMessage());
                    }
                }
        );
        queue.add(simpleRequest);
    }

    public static void dailyLecturasPush() {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(MyApplication.getAppContext())
                        .setSmallIcon(R.drawable.ic_action_book)
                        .setContentTitle("Lee la lectura de hoy!")
                        .setSound(uri);
        Intent resultIntent = new Intent(MyApplication.getAppContext(), Lecturas.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(MyApplication.getAppContext());
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) MyApplication.getAppContext().getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(mid++, mBuilder.build());
    }

    private static void sendShortPush(String contentTitle, String contentText, Uri uriResource, String imageUrl) {

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(MyApplication.getAppContext())
                        .setSmallIcon(R.drawable.ic_action_book)
                        .setContentTitle("Nuevas noticias!")
                        .setSound(uriResource)
                        .setContentText(contentTitle);
        Intent resultIntent = new Intent(MyApplication.getAppContext(), NewsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("title", contentTitle);
        bundle.putString("content", contentText);
        bundle.putString("image", imageUrl);
        resultIntent.putExtra("DisplayDataBundle", bundle);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(MyApplication.getAppContext());
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) MyApplication.getAppContext().getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(mid++, mBuilder.build());
    }
}
