package com.landagmail.josemaria.appprojectreligion.POJO.BibleUtils;

import java.io.Serializable;
import java.util.ArrayList;

public class BibleSearchData implements Serializable{

    private String title;
    private String preview;

    private ArrayList<String> titles = new ArrayList<>();
    private ArrayList<String> previews = new ArrayList<>();

    public BibleSearchData() {
    }

    public void setTitle(String title) {
        this.title = title;
        titles.add(titles.size(), title);
    }

    public void setPreview(String preview) {
        this.preview = preview;
        previews.add(previews.size(), preview);
    }

    public ArrayList<String> getPreviews() {
        return previews;
    }

    public ArrayList<String> getTitles() {
        return titles;
    }

}
