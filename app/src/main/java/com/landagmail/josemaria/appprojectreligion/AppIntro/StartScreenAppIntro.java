package com.landagmail.josemaria.appprojectreligion.AppIntro;

import android.content.Intent;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro2;
import com.landagmail.josemaria.appprojectreligion.Activities.UsersActivities.SignInActivity;
import com.landagmail.josemaria.appprojectreligion.R;

public class StartScreenAppIntro extends AppIntro2 {

    @Override
    public void init(Bundle savedInstanceState) {
        addSlide(IntroSlides.newInstance(R.layout.intro2));
        addSlide(IntroSlides.newInstance(R.layout.intro2_2));
        addSlide(IntroSlides.newInstance(R.layout.intro3_2));
    }

    private void loadHomeScreenActivity(){
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onDonePressed() {
        loadHomeScreenActivity();
    }
}
