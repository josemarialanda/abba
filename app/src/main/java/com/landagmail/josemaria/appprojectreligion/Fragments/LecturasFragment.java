package com.landagmail.josemaria.appprojectreligion.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.landagmail.josemaria.appprojectreligion.Activities.SettingsActivity;
import com.landagmail.josemaria.appprojectreligion.Constants;
import com.landagmail.josemaria.appprojectreligion.Logging.L;
import com.landagmail.josemaria.appprojectreligion.Network.SimpleXmlRequest;
import com.landagmail.josemaria.appprojectreligion.Network.VolleySingleton;
import com.landagmail.josemaria.appprojectreligion.POJO.RssCommon.RSS;
import com.landagmail.josemaria.appprojectreligion.R;

public class LecturasFragment extends Fragment {


    public LecturasFragment() {
    }

    private ObservableScrollView scrollView;
    public static final String ARG_SCROLL_Y = "ARG_SCROLL_Y";

    TextView rssDescription;
    private RequestQueue queue;
    private int TextSize = 20;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lecturas, container, false);

        scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
        Activity parentActivity = getActivity();
        if (parentActivity instanceof ObservableScrollViewCallbacks) {
            // Scroll to the specified offset after layout
            Bundle args = getArguments();
            if (args != null && args.containsKey(ARG_SCROLL_Y)) {
                final int scrollY = args.getInt(ARG_SCROLL_Y, 0);
                ScrollUtils.addOnGlobalLayoutListener(scrollView, new Runnable() {
                    @Override
                    public void run() {
                        scrollView.scrollTo(0, scrollY);
                    }
                });
            }

            scrollView.setTouchInterceptionViewGroup((ViewGroup) parentActivity.findViewById(R.id.root));

            scrollView.setScrollViewCallbacks((ObservableScrollViewCallbacks) parentActivity);
        }

        rssDescription = (TextView) view.findViewById(R.id.rssDescriptionLectura);
        readPreferences();
        new LecturasTask().execute(Constants.LECTURAS_DIARIAS_URL);

        return view;
    }

    private class LecturasTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            queue = VolleySingleton.getInstance().getRequestQueue();

            SimpleXmlRequest<RSS> simpleRequest = new SimpleXmlRequest<RSS>(Request.Method.GET, params[0], RSS.class,
                    new Response.Listener<RSS>() {
                        @Override
                        public void onResponse(RSS response) {
                            String title = response.getChannel().itemList.get(0).getTitle();
                            String description = response.getChannel().itemList.get(0).getDescription();
                            onProgressUpdate(title, description);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            L.t(error.getMessage());
                        }
                    }
            );
            queue.add(simpleRequest);
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {

            rssDescription.setText(values[0] + "\n\n " + Html.fromHtml(values[1]));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_missal, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.fontSizeBigger) {
            if (TextSize > 13 && TextSize < 25) {
                TextSize++;
                rssDescription.setTextSize(TextSize);
                //L.t(String.valueOf(TextSize));
            }
        }
        if (id == R.id.fontSizeSmaller) {
            if (TextSize > 13 && TextSize < 25) {
                TextSize--;
                rssDescription.setTextSize(TextSize);
                //L.t(String.valueOf(TextSize));
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        textSizePrefs = getActivity().getSharedPreferences(FONT_SIZE_PREFS, Context.MODE_PRIVATE);
        TextEditorPrefs = textSizePrefs.edit();
        TextEditorPrefs.putInt(FONT_SIZE, TextSize);
        TextEditorPrefs.apply();
        Log.i("cool", "onPause " + TextSize);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("cool", "onResume " + TextSize);
    }

    private SharedPreferences textSizePrefs;
    private static final String FONT_SIZE_PREFS = "textPrefs";
    private SharedPreferences.Editor TextEditorPrefs;
    private static final String FONT_SIZE = "textSize";

    private void readPreferences() {
        textSizePrefs = getActivity().getSharedPreferences(FONT_SIZE_PREFS, Context.MODE_PRIVATE);
        TextSize = textSizePrefs.getInt(FONT_SIZE, 20);
        rssDescription.setTextSize(TextSize);
    }
}
