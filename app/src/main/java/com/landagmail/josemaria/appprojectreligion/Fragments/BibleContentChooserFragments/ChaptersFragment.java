package com.landagmail.josemaria.appprojectreligion.Fragments.BibleContentChooserFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.landagmail.josemaria.appprojectreligion.Adapters.ChaptersAdapter;
import com.landagmail.josemaria.appprojectreligion.DataBase.SqlLiteDbHelperBIBLE;
import com.landagmail.josemaria.appprojectreligion.R;

import java.util.ArrayList;

public class ChaptersFragment extends Fragment {

    public ChaptersFragment() {
    }

    public RecyclerView chaptersList;
    private ChaptersAdapter adapter;
    ArrayList<Integer> chapters;
    SqlLiteDbHelperBIBLE dbHelperBIBLE;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbHelperBIBLE = new SqlLiteDbHelperBIBLE(getActivity());
        dbHelperBIBLE.openDataBase();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chapters, container, false);

        chaptersList = (RecyclerView) view.findViewById(R.id.chaptersList);
        chaptersList.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new ChaptersAdapter(getActivity(), ChaptersFragment.this);
        chaptersList.setAdapter(adapter);

        return view;
    }

    public void recieveChaptersData(String book) {
        chapters = (ArrayList<Integer>) dbHelperBIBLE.getChapterRows(book);
        adapter.setChaptersData(chapters, book);
    }
}
