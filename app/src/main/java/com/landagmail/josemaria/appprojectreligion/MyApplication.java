package com.landagmail.josemaria.appprojectreligion;
//Main Application Class
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;

import android.util.Base64;
import android.util.Log;
import android.widget.Toast;
import com.crashlytics.android.Crashlytics;

import com.landagmail.josemaria.appprojectreligion.AppIntro.StartScreenAppIntro;
import com.landagmail.josemaria.appprojectreligion.Utils.OnCrashActivity.CustomActivityOnCrash;
import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.UAirship;

import io.fabric.sdk.android.Fabric;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

//650768366091

public class MyApplication extends Application {

    private static MyApplication appInstance;

    public String getChannelId() {
        return channelId;
    }

    private String channelId;

    @Override
    public void onCreate() {
        super.onCreate();
        //CustomActivityOnCrash.install(this);
        Fabric.with(this, new Crashlytics());
        appInstance = this;
        //printHashKey();

        AirshipConfigOptions options = new AirshipConfigOptions();
        options.developmentAppKey = "74NvghOMRmKJlwG7l-4OGQ";
        options.developmentAppSecret = "AjEV3i2cRkug9OxjtNeqTQ";
        options.gcmSender = "650768366091";
        options.productionAppKey = "";
        options.productionAppSecret = "";
        options.inProduction = false;
        UAirship.takeOff(this, options);
        channelId = UAirship.shared().getPushManager().getChannelId();
        UAirship.shared().getPushManager().setUserNotificationsEnabled(true);

        Boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                .getBoolean("isFirstRun", true);

        if (isFirstRun) {

            Intent intent = new Intent(this, StartScreenAppIntro.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            Toast.makeText(this, "First Run", Toast.LENGTH_LONG)
                    .show();
        }

        getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit()
                .putBoolean("isFirstRun", false).commit();
    }

    public static MyApplication getInstance(){
        return appInstance;
    }

    public static Context getAppContext(){
        return appInstance.getApplicationContext();
    }

    public void printHashKey(){

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.landagmail.josemaria.appprojectreligion",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
}
