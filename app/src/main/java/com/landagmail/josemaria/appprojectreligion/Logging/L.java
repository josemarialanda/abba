package com.landagmail.josemaria.appprojectreligion.Logging;

import android.widget.Toast;

import com.landagmail.josemaria.appprojectreligion.MyApplication;

import java.util.ArrayList;

public class L {

    private L(){

    }

    public static void t(String message){
        Toast.makeText(MyApplication.getAppContext(), message + "", Toast.LENGTH_SHORT).show();
    }
    public static void m(String message){
        Toast.makeText(MyApplication.getAppContext(), message + "", Toast.LENGTH_LONG).show();
    }

    public static void loop(ArrayList<String> list){
        StringBuilder builder = new StringBuilder();
        for (String  b : list) {
            builder.append(b).append("\n");
        }
        Toast.makeText(MyApplication.getAppContext(), builder + "", Toast.LENGTH_LONG).show();
    }
}
