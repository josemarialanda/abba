package com.landagmail.josemaria.appprojectreligion.DataBase;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SqlLiteDbHelperBIBLE extends SQLiteOpenHelper {

    private static final String TABLE = "bible_fts_content";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "rva.db";
    private static final String DB_PATH_SUFFIX = "/databases/";
    static Context ctx;

    public SqlLiteDbHelperBIBLE(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        ctx = context;
    }

    public void CopyDataBaseFromAsset() throws IOException {
        InputStream myInput = ctx.getAssets().open(DATABASE_NAME);
        String outFileName = getDatabasePath();
        File f = new File(ctx.getApplicationInfo().dataDir + DB_PATH_SUFFIX);
        if (!f.exists())
            f.mkdir();
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    private static String getDatabasePath() {
        return ctx.getApplicationInfo().dataDir + DB_PATH_SUFFIX + DATABASE_NAME;
    }

    public SQLiteDatabase openDataBase() throws SQLException {
        File dbFile = ctx.getDatabasePath(DATABASE_NAME);
        if (!dbFile.exists()) {
            try {
                CopyDataBaseFromAsset();
                System.out.println("Copying sucess from Assets folder");
            } catch (IOException e) {
                throw new RuntimeException("Error creating source database", e);
            }
        }
        return SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.CREATE_IF_NECESSARY);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    /*                       SELECT METHODS                          */

    public String[] getColumnNames() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE + " LIMIT 1", null);
        String[] colNames = cursor.getColumnNames();
        return colNames;
    }

    public List<String> getBookRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT DISTINCT " + "c0book" + " FROM " + TABLE, null);
        List<String> list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                list.add(cursor.getString(cursor.getColumnIndex("c0book")));
            }
            while (cursor.moveToNext());
        }
        return list;
    }

    public List<Integer> getChapterRows(String book) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT DISTINCT c1chapter FROM " + TABLE + " WHERE c0book = '" + book + "'", null);
        List<Integer> list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                list.add(cursor.getInt(cursor.getColumnIndex("c1chapter")));
            }
            while (cursor.moveToNext());
        }
        return list;
    }

    public String getBibleContent(String book, int chapter) {
        StringBuffer buffer = new StringBuffer();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT c3content FROM " + TABLE + " WHERE c0book = '" + book + "' " + "AND " + "c1chapter = '" + chapter + "'", null);
        if (cursor.moveToFirst()) {
            int counter = 1;
            do {
                buffer.append(counter++ + " " + cursor.getString(cursor.getColumnIndex("c3content")) + "\n\n");
            }
            while (cursor.moveToNext());
        }
        return buffer.toString();
    }

    public String getRandomBibleVerse(){
        SQLiteDatabase db = this.getReadableDatabase();
        Random random = new Random();
        StringBuffer buffer = new StringBuffer();
        Cursor cursor = db.rawQuery("SELECT c3content FROM " + TABLE + " WHERE docid = '" + String.valueOf(random.nextInt(31101)) + "'", null);
        if (cursor.moveToFirst()) {
            do {
                buffer.append(cursor.getString(cursor.getColumnIndex("c3content")));
            }
            while (cursor.moveToNext());
        }
        return buffer.toString();
    }
}

















