package com.landagmail.josemaria.appprojectreligion.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.landagmail.josemaria.appprojectreligion.R;

import java.io.InputStream;

public class Missal extends ActionBarActivity {

    private TextView myText;
    private SharedPreferences textSizePrefs;
    private static final String FONT_SIZE_PREFS = "textPrefs";
    private SharedPreferences.Editor TextEditorPrefs;
    private static final String FONT_SIZE = "textSize";
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_missal);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Missal");

        myText = (TextView) findViewById(R.id.myText);

        readPreferences();

        byte[] buffer = new byte[0];
        try {
            InputStream is = getAssets().open("misalHtml.html");
            int size = is.available();
            buffer = new byte[size];
            is.read(buffer);
            is.close();
        } catch (Exception e) {}

        String str = new String(buffer);
        myText.setText(Html.fromHtml(str));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_missal, menu);
        return true;
    }

    private int TextSize = 20;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.fontSizeBigger) {
            if (TextSize > 13 && TextSize <25){
                TextSize++;
                myText.setTextSize(TextSize);
                //L.t(String.valueOf(TextSize));
            }
        }
        if (id == R.id.fontSizeSmaller) {
            if (TextSize > 13 && TextSize <25){
                TextSize--;
                myText.setTextSize(TextSize);
                //L.t(String.valueOf(TextSize));
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        textSizePrefs = getSharedPreferences(FONT_SIZE_PREFS, Context.MODE_PRIVATE);
        TextEditorPrefs = textSizePrefs.edit();
        TextEditorPrefs.putInt(FONT_SIZE, TextSize);
        TextEditorPrefs.apply();
        Log.i("cool", "onPause " + TextSize);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("cool", "onResume " + TextSize);
    }

    private void readPreferences() {
        textSizePrefs = getSharedPreferences(FONT_SIZE_PREFS, Context.MODE_PRIVATE);
        TextSize = textSizePrefs.getInt(FONT_SIZE, 20);
        myText.setTextSize(TextSize);
    }
}
