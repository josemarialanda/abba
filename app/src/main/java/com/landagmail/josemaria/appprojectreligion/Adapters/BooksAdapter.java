package com.landagmail.josemaria.appprojectreligion.Adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.landagmail.josemaria.appprojectreligion.Activities.BibleContentChooserActivity;
import com.landagmail.josemaria.appprojectreligion.Fragments.BibleContentChooserFragments.BooksFragment;
import com.landagmail.josemaria.appprojectreligion.R;

import java.util.ArrayList;

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.myViewHolder> implements View.OnClickListener {

    private LayoutInflater layoutInflater;
    Context context;
    private ArrayList<String> booksDataArrayList = new ArrayList<>();
    BooksFragment booksFragmentCtxt;


    public BooksAdapter(FragmentActivity activity, BooksFragment booksFragment) {
        layoutInflater = LayoutInflater.from(activity);
        context = activity;
        booksFragmentCtxt = booksFragment;
    }


    public void setBooksData(ArrayList<String> booksData){
        booksDataArrayList = booksData;
        notifyDataSetChanged();
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.books_fragment_list_view, parent, false);
        myViewHolder viewHolder = new myViewHolder(view);
        view.setOnClickListener(this);
        return viewHolder;
    }

    @Override
    public void onClick(View v) {
        int position = booksFragmentCtxt.bookList.getChildPosition(v);
        ((BibleContentChooserActivity) booksFragmentCtxt.getActivity()).setSelectedBook(booksDataArrayList.get(position));
        ((BibleContentChooserActivity) booksFragmentCtxt.getActivity()).setViewPagerPosition(1);
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {
        String book = booksDataArrayList.get(position);
        holder.selectedBook.setText(book);
    }

    @Override
    public int getItemCount() {
        return booksDataArrayList.size();
    }

    static class myViewHolder extends RecyclerView.ViewHolder{

        TextView selectedBook;

        public myViewHolder(View itemView) {
            super(itemView);
            selectedBook = (TextView) itemView.findViewById(R.id.selectedBook);
        }
    }
}
