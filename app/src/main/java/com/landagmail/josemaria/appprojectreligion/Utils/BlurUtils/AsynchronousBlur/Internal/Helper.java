package com.landagmail.josemaria.appprojectreligion.Utils.BlurUtils.AsynchronousBlur.Internal;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;


public final class Helper {

    public static void setBackground(View v, Drawable drawable) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            v.setBackground(drawable);
        } else {
            v.setBackgroundDrawable(drawable);
        }
    }

    public static boolean hasZero(int... args) {
        for (int num : args) {
            if (num == 0) {
                return true;
            }
        }
        return false;
    }
}