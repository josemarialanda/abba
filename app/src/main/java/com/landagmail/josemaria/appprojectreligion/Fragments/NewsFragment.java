package com.landagmail.josemaria.appprojectreligion.Fragments;
//NewsFragment

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.landagmail.josemaria.appprojectreligion.Activities.NewsActivity;
import com.landagmail.josemaria.appprojectreligion.Activities.SettingsActivity;
import com.landagmail.josemaria.appprojectreligion.BaseClasses.BaseFragment;
import com.landagmail.josemaria.appprojectreligion.Constants;
import com.landagmail.josemaria.appprojectreligion.Logging.L;
import com.landagmail.josemaria.appprojectreligion.Network.SimpleXmlRequest;
import com.landagmail.josemaria.appprojectreligion.Network.VolleySingleton;
import com.landagmail.josemaria.appprojectreligion.POJO.RssCommon.Channel;
import com.landagmail.josemaria.appprojectreligion.POJO.RssCommon.RSS;
import com.landagmail.josemaria.appprojectreligion.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class NewsFragment extends BaseFragment {

    public static final String ARG_INITIAL_POSITION = "ARG_INITIAL_POSITION";

    private RequestQueue queue;
    private SimpleHeaderRecyclerAdapter adapter;
    protected RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<Channel.Item> newsDataList = new ArrayList<>();

    String[] feeds = {Constants.SANTO_DEL_DIA, Constants.NEWS_FEED_URL__PERFILES, Constants.NEWS_FEED_URL_AMERICA, Constants.NEWS_FEED_URL_EVENTOS_CATOLICOS, Constants.NEWS_FEED_URL_INTERNET,
            Constants.NEWS_FEED_URL_MUNDO, Constants.NEWS_FEED_URL_VATICANO, Constants.NEWS_FEED_URL_VIDA_Y_FAMILIA};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_news_fragment, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(getActivity(), SettingsActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);

        Activity parentActivity = getActivity();
        recyclerView = (RecyclerView) view.findViewById(R.id.newsScroller);
        recyclerView.setLayoutManager(new LinearLayoutManager(parentActivity));
        setData(recyclerView);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        return view;
    }

    private void refreshItems() {
        new NewsFeedTask().execute(feeds);
    }

    protected void setData(RecyclerView recyclerView) {
        adapter = new SimpleHeaderRecyclerAdapter(getActivity(), newsDataList, NewsFragment.this);
        recyclerView.setAdapter(adapter);
        new NewsFeedTask().execute(feeds);
    }

    private class NewsFeedTask extends AsyncTask<String[], List<Channel.Item>, String> {

        SimpleHeaderRecyclerAdapter adapter;

        @Override
        protected String doInBackground(String[]... params) {
            queue = VolleySingleton.getInstance().getRequestQueue();

            for (int i = 0; i < feeds.length; i++) {

                SimpleXmlRequest<RSS> simpleRequest = new SimpleXmlRequest<RSS>(Request.Method.GET, feeds[i], RSS.class,
                        new Response.Listener<RSS>() {
                            @Override
                            public void onResponse(RSS response) {
                                onProgressUpdate(response.getChannel().itemList);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                L.t(error.getMessage());
                            }
                        }
                );
                queue.add(simpleRequest);
            }
            return "Operation successful";
        }

        private ArrayList<Channel.Item> list = new ArrayList<>();

        @Override
        protected void onProgressUpdate(List<Channel.Item>... values) {
            list.addAll(list.size(), values[0]);
            adapter.setData(list);
        }

        @Override
        protected void onPreExecute() {
            adapter = (SimpleHeaderRecyclerAdapter) recyclerView.getAdapter();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            swipeRefreshLayout.setRefreshing(false);
        }
    }

}

class SimpleHeaderRecyclerAdapter extends RecyclerView.Adapter<SimpleHeaderRecyclerAdapter.myViewHolder> implements View.OnClickListener {

    private ArrayList<Channel.Item> data;
    private LayoutInflater layoutInflater;
    Context context;
    NewsFragment newsFragmentContext;
    private ImageLoader mImageLoader;
    private VolleySingleton mVolleySingleton;

    public SimpleHeaderRecyclerAdapter(FragmentActivity activity, ArrayList<Channel.Item> newsDataList, NewsFragment newsFragmentContext) {
        data = newsDataList;
        context = activity;
        this.newsFragmentContext = newsFragmentContext;
        layoutInflater = LayoutInflater.from(context);
        mVolleySingleton = VolleySingleton.getInstance();
        mImageLoader = mVolleySingleton.getImageLoader();
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.news_fragment_list_view, parent, false);
        myViewHolder viewHolder = new myViewHolder(view);

        view.setOnClickListener(this);
        return viewHolder;
    }

    @Override
    public void onClick(View v) {
        int position = newsFragmentContext.recyclerView.getChildPosition(v);

        String url = data.get(position).getEnclosure().getUrl();
        String content = data.get(position).getEncoded().replaceAll("http:\\/\\/(.*?)[^\\\"']+", "")
                .replace("<img src=\"\" />", "")
                .replace("<img src=\"\" height=\"1\" width=\"1\" alt=\"\"/>", "");

        String title = data.get(position).getTitle();
        String link = data.get(position).getLink();

        //L.m(data.get(position).getPubSimpleDate().toString());
        Intent intent = new Intent(context, NewsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("image", url);
        bundle.putString("link", link);
        bundle.putString("title", title);
        bundle.putString("content", content);
        intent.putExtra("DisplayDataBundle", bundle);
        context.startActivity(intent);
    }

    int position;

    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        this.position = position;
        String title = data.get(position).getTitle();
        holder.rssTitle.setText(title);
        String imageUrl = data.get(position).getEnclosure().getUrl();
        loadImages(imageUrl, holder);
    }

    private void loadImages(String urlThumbnail, final myViewHolder holder) {
        mImageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                holder.rssImage.setImageBitmap(response.getBitmap());
            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(ArrayList<Channel.Item> list) {
        data = list;

        Collections.sort(data, new Comparator<Channel.Item>() {
            @Override
            public int compare(Channel.Item item1, Channel.Item item2) {

                return item1.getPubSimpleDate().compareTo(item2.getPubSimpleDate());
            }
        });

        Collections.reverse(data);
        notifyDataSetChanged();
    }

    static class myViewHolder extends RecyclerView.ViewHolder {

        ImageView rssImage;
        TextView rssTitle;

        public myViewHolder(View itemView) {
            super(itemView);

            rssTitle = (TextView) itemView.findViewById(R.id.rssTitle);
            rssImage = (ImageView) itemView.findViewById(R.id.rssImage);

        }
    }
}

