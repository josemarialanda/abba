package com.landagmail.josemaria.appprojectreligion.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.landagmail.josemaria.appprojectreligion.Constants;
import com.landagmail.josemaria.appprojectreligion.Logging.L;
import com.landagmail.josemaria.appprojectreligion.Network.SimpleXmlRequest;
import com.landagmail.josemaria.appprojectreligion.Network.VolleySingleton;
import com.landagmail.josemaria.appprojectreligion.POJO.RssCommon.RSS;
import com.landagmail.josemaria.appprojectreligion.R;

public class Lecturas extends ActionBarActivity {

    TextView rssTitle;
    TextView rssDescription;
    private RequestQueue queue;
    private Toolbar toolbar;
    private int TextSize = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lecturas);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Lecturas");

        rssTitle = (TextView) findViewById(R.id.rssTitleLectura);
        rssDescription = (TextView) findViewById(R.id.rssDescriptionLectura);

        readPreferences();

        new LecturasTask().execute(Constants.LECTURAS_DIARIAS_URL);
    }

    private class LecturasTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            queue = VolleySingleton.getInstance().getRequestQueue();

            SimpleXmlRequest<RSS> simpleRequest = new SimpleXmlRequest<RSS>(Request.Method.GET, params[0], RSS.class,
                    new Response.Listener<RSS>() {
                        @Override
                        public void onResponse(RSS response) {
                            String title = response.getChannel().itemList.get(0).getTitle();
                            String description = response.getChannel().itemList.get(0).getDescription();
                            onProgressUpdate(title, description);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            L.t(error.getMessage());
                        }
                    }
            );
            queue.add(simpleRequest);
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            rssTitle.setText(Html.fromHtml(values[0]));
            rssDescription.setText(Html.fromHtml(values[1]));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_missal, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.fontSizeBigger) {
            if (TextSize > 13 && TextSize <25){
                TextSize++;
                rssDescription.setTextSize(TextSize);
                //L.t(String.valueOf(TextSize));
            }
        }
        if (id == R.id.fontSizeSmaller) {
            if (TextSize > 13 && TextSize <25){
                TextSize--;
                rssDescription.setTextSize(TextSize);
                //L.t(String.valueOf(TextSize));
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        textSizePrefs = getSharedPreferences(FONT_SIZE_PREFS, Context.MODE_PRIVATE);
        TextEditorPrefs = textSizePrefs.edit();
        TextEditorPrefs.putInt(FONT_SIZE, TextSize);
        TextEditorPrefs.apply();
        Log.i("cool", "onPause " + TextSize);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("cool", "onResume " + TextSize);
    }

    private SharedPreferences textSizePrefs;
    private static final String FONT_SIZE_PREFS = "textPrefs";
    private SharedPreferences.Editor TextEditorPrefs;
    private static final String FONT_SIZE = "textSize";

    private void readPreferences() {
        textSizePrefs = getSharedPreferences(FONT_SIZE_PREFS, Context.MODE_PRIVATE);
        TextSize = textSizePrefs.getInt(FONT_SIZE, 20);
        rssDescription.setTextSize(TextSize);
    }
}
