package com.landagmail.josemaria.appprojectreligion;

public class Constants {

    public static final String URL_BOOK_CHAPTER_BIBLE = "https://api.biblia.com/v1/bible/contents/LEB?key=";
    public static final String BIBLE_API_KEY = "b8491746c3745e3c9861bdd1a90d91bf";
    public static final String LECTURAS_DOMINICALES_URL = "http://www.conjesus.org/rss/liturgias.xml";

    public static final String BIBLE_RVR60 = "rvr60";
    public static final String BIBLE_RVA = "rva";
    public static final String BIBLE_LEB = "LEB";
    public static final String BIBLE_KJV = "kjv";

    public static final String DISPLAY_DATA_URL_PART_1 = "http://api.biblia.com/v1/bible/content/rvr60.txt.json?passage=";
    //Missing data = (passage)
    public static final String DISPLAY_DATA_URL_PART_2 = "&style=orationOneVersePerLine&key=";

    public static final String LECTURAS_DIARIAS_URL = "http://feeds.feedburner.com/LecturasEvangelioMisaDia?format=xml";

    public static final String MEDITACION_DOMINICAL = "http://feeds.feedburner.com/aciprensa-meditaciondominical?format=xml";

    public static final String SANTO_DEL_DIA = "http://feeds.feedburner.com/aciprensa-santodeldia?format=xml";
    public static final String NEWS_FEED_URL_VATICANO = "http://feeds.feedburner.com/noticiasaci-vaticano?format=xml";
    public static final String NEWS_FEED_URL_AMERICA = "http://feeds.feedburner.com/noticiasaci-america?format=xml";
    public static final String NEWS_FEED_URL_MUNDO = "http://feeds.feedburner.com/noticiasaci-mundo?format=xml";
    public static final String NEWS_FEED_URL_VIDA_Y_FAMILIA = "http://feeds.feedburner.com/noticiasaci-vidayfamilia?format=xml";
    public static final String NEWS_FEED_URL__PERFILES = "http://feeds.feedburner.com/noticiasaci-perfiles?format=xml";
    public static final String NEWS_FEED_URL_EVENTOS_CATOLICOS = "http://feeds.feedburner.com/noticiasaci-eventoscatolicos?format=xml";
    public static final String NEWS_FEED_URL_INTERNET = "http://feeds.feedburner.com/noticiasaci-internet?format=xml";

    public static final String BIBLE_SEARCH_PART_1 = "https://api.biblia.com/v1/bible/search/";
    //public static final String BIBLE_RVR60 = "rva";
    public static final String BIBLE_SEARCH_PART_2 = "?query="; // EG: pan
    public static final String BIBLE_SEARCH_PART_4 = "&mode=verse&key=";
    //public static final String BIBLE_API_KEY = "b8491746c3745e3c9861bdd1a90d91bf";
    public static final String CULTURE_SPEC = "&culture=es";


    public interface Keys {
        interface Books {
            String KEY_BOOK = "books";
            String KEY_CURRENT_BOOK = "passage";
            String KEY_SELECTED_CHAPTER = "text";
        }
        interface Chapters {
            String KEY_CHAPTERS_ARRAY = "chapters";
            String KEY_CHAPTERS = "passage";
        }
    }
}
