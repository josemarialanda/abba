package com.landagmail.josemaria.appprojectreligion.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.landagmail.josemaria.appprojectreligion.Fragments.BibleContentChooserFragments.BooksFragment;
import com.landagmail.josemaria.appprojectreligion.Fragments.BibleContentChooserFragments.ChaptersFragment;
import com.landagmail.josemaria.appprojectreligion.Logging.L;
import com.landagmail.josemaria.appprojectreligion.R;
import com.landagmail.josemaria.appprojectreligion.Utils.Tabs.SlidingTabLayout;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;


public class BibleContentChooserActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ViewPager mPager;
    private SlidingTabLayout mTabs;
    private BooksFragment booksFragment;
    private ChaptersFragment chaptersFragment;
    private SlidingUpPanelLayout mLayout;
    private String book;

    boolean isSliderOpen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bible_content_chooser);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        mPager = (ViewPager) findViewById(R.id.home_fragment_pager_bibleContentChooser);
        FragmentManager manager = getSupportFragmentManager();
        mPager.setAdapter(new HomePagerAdapter(manager));
        mPager.setOffscreenPageLimit(2);
        mTabs = (SlidingTabLayout) findViewById(R.id.home_fragment_tabs_bibleContentChooser);
        mTabs.setDistributeEvenly(true);
        mTabs.setSelectedIndicatorColors(getResources().getColor(R.color.accentColor));
        mTabs.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        mTabs.setViewPager(mPager);

        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);

        mLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
            }

            @Override
            public void onPanelExpanded(View panel) {
                isSliderOpen = true;
            }

            @Override
            public void onPanelCollapsed(View panel) {
                isSliderOpen = false;
            }

            @Override
            public void onPanelAnchored(View panel) {

            }

            @Override
            public void onPanelHidden(View panel) {
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            Log.i("chooserClass", "onBackPressed");
        }

        return super.onOptionsItemSelected(item);
    }

    public void setViewPagerPosition(int pagerPosition) {
        mPager.setCurrentItem(pagerPosition);
    }

    public void setSelectedBook(String book) {
        this.book = book;
        sendData(book);
    }

    private void sendData(String book) {
        chaptersFragment.recieveChaptersData(book);
    }

    public void closeSlider(){
        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        isSliderOpen = false;
    }

    @Override
    public void onBackPressed() {

        if (isSliderOpen) {
            closeSlider();
        } else {
            if (book == null) {
                setResult(Activity.RESULT_CANCELED);
                super.onBackPressed();
            }

            L.m(book + "\n" + chapter);

            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putString("book", book);
            bundle.putInt("chapter", chapter);
            intent.putExtra("displayDataInfoBundle", bundle);
            if (getParent() != null) {
                getParent().setResult(Activity.RESULT_OK, intent);
            } else {
                setResult(Activity.RESULT_OK, intent);
            }
            super.onBackPressed();
        }
    }

    private int chapter;

    public void setData(String book, Integer chapter) {
        this.book = book;
        this.chapter = chapter;
        onBackPressed();
    }


    private class HomePagerAdapter extends FragmentStatePagerAdapter {

        private String[] tabs = {"Books", "Chapters"};
        int length = tabs.length;

        public HomePagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
            booksFragment = new BooksFragment();
            chaptersFragment = new ChaptersFragment();
        }

        @Override
        public int getCount() {
            return length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: // Fragment # 0
                    return booksFragment;
                case 1: // Fragment # 1
                    return chaptersFragment;
                default:// Fragment # 0
                    return null;
            }
        }
    }
}
