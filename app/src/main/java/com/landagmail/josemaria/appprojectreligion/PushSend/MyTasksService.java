package com.landagmail.josemaria.appprojectreligion.PushSend;

import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;

public class MyTasksService extends GcmTaskService {

    public MyTasksService() {
        super();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.i("myTaskService", "onBind");
        return super.onBind(intent);
    }

    @Override
    public void onInitializeTasks() {
        super.onInitializeTasks();
        Log.i("myTaskService", "onInitializeTasks");
    }

    @Override
    public int onRunTask(TaskParams taskParams) {
        Log.i("myTaskService", "onRunTask");
        SendPushNotification.dailyNewsPush();
        SendPushNotification.dailyLecturasPush();
        return 0;
    }
}