package com.landagmail.josemaria.appprojectreligion.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.landagmail.josemaria.appprojectreligion.Activities.BibleContentChooserActivity;
import com.landagmail.josemaria.appprojectreligion.Fragments.BibleContentChooserFragments.ChaptersFragment;
import com.landagmail.josemaria.appprojectreligion.R;

import java.util.ArrayList;

public class ChaptersAdapter extends RecyclerView.Adapter<ChaptersAdapter.myViewHolder> implements View.OnClickListener {

    private LayoutInflater layoutInflater;
    Context context;
    private ArrayList<Integer> chaptersDataArrayList = new ArrayList<>();
    ChaptersFragment chaptersFragmentCtxt;
    private String book;

    public ChaptersAdapter(FragmentActivity activity, ChaptersFragment chaptersFragment) {
        layoutInflater = LayoutInflater.from(activity);
        context = activity;
        chaptersFragmentCtxt = chaptersFragment;
    }

    public void setChaptersData(ArrayList<Integer> chaptersData, String book) {
        chaptersDataArrayList = chaptersData;
        this.book = book;
        notifyDataSetChanged();
    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.chapters_fragment_list_view, parent, false);
        myViewHolder viewHolder = new myViewHolder(view);
        view.setOnClickListener(this);
        return viewHolder;
    }

    @Override
    public void onClick(View v) {
        int position = chaptersFragmentCtxt.chaptersList.getChildPosition(v);
        ((BibleContentChooserActivity) chaptersFragmentCtxt.getActivity()).setData(book, chaptersDataArrayList.get(position));
        //chaptersFragmentCtxt.getActivity().onBackPressed();
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {
        int chapter = chaptersDataArrayList.get(position);
        holder.selectedChapter.setText(String.valueOf(chapter));
    }

    @Override
    public int getItemCount() {
        return chaptersDataArrayList.size();
    }

    static class myViewHolder extends RecyclerView.ViewHolder {

        TextView selectedChapter;

        public myViewHolder(View itemView) {
            super(itemView);
            selectedChapter = (TextView) itemView.findViewById(R.id.selectedChapter);
        }
    }
}
